package com.example.jfreechartandroid;

import android.support.v7.app.ActionBarActivity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

public class MainActivity extends ActionBarActivity {
	ImageView imageView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		imageView=(ImageView) findViewById(R.id.chartImageView);
	}
	
	@Override
	protected void onStart() {
		super.onStart();
		
		LineChart chart=new LineChart();
		Bitmap bitmap=chart.addData(10);
		imageView.setImageBitmap(bitmap);
	}

}
