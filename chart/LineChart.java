package com.example.jfreechartandroid;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class LineChart {
	private DefaultCategoryDataset dataset;
	private JFreeChart chart;
	private OutputStream outPutStream;

	public LineChart() {
		this.dataset=new DefaultCategoryDataset();
		this.dataset.addValue(0, "A君", "0");
		createChart();

	}

	private void createChart() {
		chart = ChartFactory.createLineChart("心拍数", "時間[m]", "回数", dataset,
				PlotOrientation.VERTICAL, true, false, false);
	}

	public Bitmap addData(int data) {
		byte[] picData = null;
		
		dataset.addValue(data, "A君", "1");
		
		try {
			ChartUtilities.writeChartAsJPEG(outPutStream, chart, 100, 100);
			outPutStream.write(picData);
			Bitmap bitmap=BitmapFactory.decodeStream(new ByteArrayInputStream(picData));
			return bitmap;
		} catch (IOException e) {
			e.printStackTrace();
		}
		//No Imageを返す
		return null;
	}
}
