package redgrape.kumikomi;

import java.io.File;

import org.afree.chart.AFreeChart;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * A placeholder fragment containing a simple view.
 */
public class PlaceholderFragment extends Fragment implements OnClickListener {
	/**
	 * The fragment argument representing the section number for this fragment.
	 */
	private static final String ARG_SECTION_NUMBER = "section_number";
	private static View rootview;

	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static PlaceholderFragment newInstance(int sectionNumber) {
		PlaceholderFragment fragment = new PlaceholderFragment();
		Bundle args = new Bundle();
		args.putInt(ARG_SECTION_NUMBER, sectionNumber);
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO 自動生成されたメソッド・スタブ
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		rootview = inflater.inflate(R.layout.fragment_main, container, false);
		((Button) rootview.findViewById(R.id.getpath)).setOnClickListener(this);
		((Button) rootview.findViewById(R.id.generate)).setOnClickListener(this);
		ChartView img=(ChartView)rootview.findViewById(R.id.chart_view);
		LineChart chart=new LineChart();
		AFreeChart bitmap=chart.addData(10);
		img.setChart(bitmap);
		return rootview;
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		((MainActivity) activity).onSectionAttached(getArguments().getInt(
				ARG_SECTION_NUMBER));
	}

	@Override
	public void onClick(View v) {
		// TODO 自動生成されたメソッド・スタブ
		if (v.getId()==R.id.getpath) {

		File file = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
		Log.d("path", file.toString());

		Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
		intent.setDataAndType(Uri.fromFile(file), "*/*");
		startActivityForResult(intent, 1);



		}else if(v.getId()==R.id.generate){
			String[] nametate=((EditText)rootview.findViewById(R.id.editText1)).getText().toString().split(",",0);

			String[] tatevalue=((EditText)rootview.findViewById(R.id.editText2)).getText().toString().split(",",0);

			String[] nameyoko=((EditText)rootview.findViewById(R.id.editText3)).getText().toString().split(",",0);

			String[] yokovalue=((EditText)rootview.findViewById(R.id.editText4)).getText().toString().split(",",0);

		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO 自動生成されたメソッド・スタブ
		super.onActivityResult(requestCode, resultCode, data);
		SharedPreferences prefs=getActivity().getSharedPreferences(getString(R.string.defo_prefs),Activity.MODE_PRIVATE);
		Editor edit=prefs.edit();
		if (requestCode == 1) {

			edit.putString(getString(R.string.filePath),data.toString());
			edit.commit();
//			((TextView)rootview.findViewById(R.id.textView1)).setText(data.getData().toString());
			((TextView)rootview.findViewById(R.id.textView1)).setText(Environment.getExternalStorageDirectory().getAbsolutePath()+"/test/");
			Toast.makeText(getActivity(), "ほぞんしたよ", Toast.LENGTH_SHORT).show();
		}
	}
}
