package redgrape.kumikomi;

import java.io.OutputStream;

import org.afree.chart.AFreeChart;
import org.afree.chart.ChartFactory;
import org.afree.chart.plot.PlotOrientation;
import org.afree.data.xy.XYSeries;
import org.afree.data.xy.XYSeriesCollection;

public class LineChart {
//	private DefaultCategoryDataset dataset;
	
	private AFreeChart chart;
	private OutputStream outPutStream;

	public LineChart() {
//		this.dataset=new DefaultCategoryDataset();
//		this.dataset.addValue(0, "A君", "0");
		createChart();

	}

	private void createChart() {
		XYSeriesCollection dataset = new XYSeriesCollection();
		XYSeries series = new XYSeries("XYSeries");
		series.add(1, 1);
		series.add(2, 2);
		series.add(3, 3);
		series.add(4, 4);
		series.add(5, 5);
		series.add(6, 6);
		series.add(7, 7);
		dataset.addSeries(series);
		XYSeries seriesw = new XYSeries("XYSSSeries");
		seriesw.add(2, 1);
		seriesw.add(22, 2);
		seriesw.add(31, 3);
		seriesw.add(41, 4);
		seriesw.add(51, 5);
		seriesw.add(61, 6);
		seriesw.add(71, 7);
		dataset.addSeries(seriesw);
		chart = ChartFactory.createXYLineChart("心拍数", "時間[m]", "回数", dataset,
				PlotOrientation.VERTICAL, true, false, false);
	}

	public AFreeChart addData(int data) {
		byte[] picData = null;

//		dataset.addValue(0.343, "A君", "23");
//		dataset.addValue(0.343, "B君", "35");
//		dataset.addValue(0.343, "A君", "345");
//		dataset.addValue(0.343, "c君", "456");
//		dataset.addValue(78, "a君", "7667");
//		dataset.addValue(56, "A君", "78978");
//		dataset.addValue(54, "A君", "187978");
//		dataset.addValue(data, "b君", "167867");


		try {
//			ChartUtilities.get(outPutStream, chart, 100, 100);
//			ChartView cha = new ChartView(, attrs)
//			cha.setChart(chart);
//			cha.getCanvas();
//			outPutStream.write(picData);
//			Bitmap bitmap=BitmapFactory.decodeStream(new ByteArrayInputStream(picData));
//			return bitmap;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return chart;
		//No Imageを返す
//		return null;
	}
}
