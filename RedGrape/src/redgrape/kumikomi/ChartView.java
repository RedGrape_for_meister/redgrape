package redgrape.kumikomi;
import org.afree.chart.AFreeChart;
import org.afree.graphics.geom.RectShape;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;


public class ChartView extends View {

    private AFreeChart chart;
    private Canvas crv;

    public ChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {

        super.onDraw(canvas);
      RectShape chartArea = new RectShape(0.0, 0.0, 400.0, 400.0);
//      		this.chart.draw(canvas, chartArea);
      		if (this.chart != null) this.chart.draw(canvas, chartArea);

        crv=canvas;

    }

    public Canvas getCanvas(){
    	return crv;
    }

    public void setChart(AFreeChart chart) {
        this.chart = chart;
    }
}