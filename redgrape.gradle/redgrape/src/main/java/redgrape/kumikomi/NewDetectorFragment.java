package redgrape.kumikomi;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.graphics.Color;
import android.hardware.usb.UsbManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.Legend;
import com.github.mikephil.charting.utils.XLabels;
import com.github.mikephil.charting.utils.YLabels;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.SerialInputOutputManager;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterApiClient;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.services.StatusesService;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.fabric.sdk.android.Fabric;

/**
 * A placeholder fragment containing a simple view.
 */
public class NewDetectorFragment extends Fragment implements View.OnClickListener {
    /**
     * The fragment argument representing the section number for this fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static View rootview;
    private Thread mThread;
    private static boolean runb = true;
    public static String ENTER_STRING = ",end";
    private static int jushin_color = 255;
    MediaPlayer mp = new MediaPlayer();

    TwitterSession mTwitterSession;


    /**
     *
     */
    //通信関連
    private SerialInputOutputManager serialIoManager;
    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    /**
     * データのbuffer
     */
    public static StringBuilder dataString = new StringBuilder();
    public static boolean flagsdata = false;

    public static StringBuilder dataStringTMP = new StringBuilder();

    private int SLEEP_TIME = 1000;//threadのスリープする時間。アニメーション時間より長めに

    private TwitterAuthClient authClient;
    /**
     * USB通信
     */
    private UsbSerialDriver usb;

    /**
     * チャート
     */
    private LineChart mChart;
    private SeekBar mSeekBarX, mSeekBarY;
    private TextView tvX, tvY;
    private Handler mHandler = new Handler();


    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    protected static final String TWITTER_KEY = "vfAvAU0Wib6nUcCVrv293mxdK";
    protected static final String TWITTER_SECRET = "HSMP1HqkCOzFE1tV0QJoI41W7ai4TL5Y7zpWpRDWzjVEG7y3BK";


    TextView si;
    TextView freqe;
    TextView conne;

    public static boolean takeALiePrev = false;

    /**
     * 嘘発見
     */
    private List<Integer> freq_list = new ArrayList<Integer>();
    private final int hold_length = 10; //保持する平均心拍数の数
    private final int threshold = 30; //嘘判定の閾値

    /**
     * Returns a new instance of this fragment for the given section number.
     */
    public static NewDetectorFragment newInstance(int sectionNumber) {
        NewDetectorFragment fragment = new NewDetectorFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * シリアル入出力 Listener
     */
    private final SerialInputOutputManager.Listener mListener =
            new SerialInputOutputManager.Listener() {
                @Override
                public void onRunError(Exception e) {
                    try {
                        usb.close();
                    } catch (IOException e1) {
                    }
                    serialIoManager.stop();
                }

                public void onNewData(final byte[] data) {

                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            while (runb == false) ;


                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    // add data
                                    try {


                                        conne.setTextColor(Color.rgb(jushin_color, jushin_color, jushin_color));
                                        setChartInfo(new String(data, "US-ASCII"));

                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }
                                    mChart.animateX(100);

                                }
                            });
                        }

                    });
                    t.run();
                }
            };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO 自動生成されたメソッド・スタブ
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(getActivity(), new Twitter(authConfig));
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //USBを繋ぐ
        UsbManager manager = (UsbManager) getActivity().getSystemService(Context.USB_SERVICE);
        usb = UsbSerialProber.acquire(manager);
        if (usb != null) {
            try {
                //listener　設定用
//                Log.i("USB","IS NOT NULL");
                serialIoManager = new SerialInputOutputManager(usb, mListener);
                executor.submit(serialIoManager);
                usb.open();
                usb.setBaudRate(9600);
//                runThread(); // シリアル通信を読むスレッドを起動
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


//        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        rootview = inflater.inflate(R.layout.new_detector_fragment, container, false);
        ((FloatingActionButton) rootview.findViewById(R.id.save_image)).setOnClickListener(this);
        ((FloatingActionButton) rootview.findViewById(R.id.startstop)).setOnClickListener(this);
        ((FloatingActionButton) rootview.findViewById(R.id.reconnect)).setOnClickListener(this);
        mChart = (LineChart) rootview.findViewById(R.id.chart1);
        mChart.setNoDataTextDescription("接続確認orアプリを再起動してね♪");
        mChart.setNoDataText("データがありません");
        si = (TextView) rootview.findViewById(R.id.trueorfalsse);
        freqe = (TextView) rootview.findViewById(R.id.sinpaku_value2);
        si.setText("アラート:OFF");
        conne = (TextView) rootview.findViewById(R.id.jushin);
        if (usb == null) {
            ((TextView) rootview.findViewById(R.id.jushin)).setText("受信エラー");
            ((TextView) rootview.findViewById(R.id.jushin)).setTextColor(getActivity().getResources().getColor(R.color.actionbar_red));
        } else {
            ((TextView) rootview.findViewById(R.id.jushin)).setTextColor(getActivity().getResources().getColor(R.color.white));
        }


//        setChartDummyInfo();
        setChartInfo("0,0\n");
        return rootview;
    }


    @Override
    public void onResume() {
        super.onResume();

    }

    /**
     * 心拍数が増加しているか確認し、アラート
     */
    private boolean isIncrease() {
        int addition = 0;

        while (freq_list.size() > hold_length) {
            freq_list.remove(0);
        }
        for (int i = 1; i < freq_list.size(); i++) {
            addition += Math.abs(freq_list.get(i - 1) - freq_list.get(i));
        }
        if (addition > threshold) return true;
        return false;
    }

    private void alertLie() {
        si.setText(getActivity().getResources().getString(R.string.arartoff));
        if (flagsdata == true) flagsdata = false;

        si.setTextColor(getActivity().getResources().getColor(R.color.white));
        if (isIncrease() != true) {
            takeALiePrev = false;
            return;
        }
        if(isIncrease()!=takeALiePrev) {
            SharedPreferences st = PreferenceManager.getDefaultSharedPreferences(getActivity());
            if (st.getBoolean("autotweet",false)&&getActivity().getSharedPreferences("com.twitter.sdk.android:twitter-core:com.twitter.sdk.android.core.TwitterCore", getActivity().MODE_PRIVATE).getString("active_twittersession", null) != null) {
                tweet("私は嘘をついているかも \n"+"現在"+((TextView) rootview.findViewById(R.id.sinpaku_value2)).getText()+" #組み込みマイスター #赤ぶどう");
            }
        }
        si.setText(getActivity().getResources().getString(R.string.ararton));
        si.setTextColor(getActivity().getResources().getColor(R.color.actionbar_red));

        if ((mp.isPlaying() == false) && (flagsdata == false)) {
            try {
                mp = MediaPlayer.create(getActivity(), R.raw.meka_ge_sindenzu02);
                mp.start();
            } catch (Exception e) {
                e.printStackTrace();

            }
            flagsdata = true;
        }
        takeALiePrev = true;
    }


    /**
     * チャートデータをセットします
     */
    private boolean setChartData(String strdata) {
        int prev = 0;
        String[] nam;
        int num = 0;
        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<Entry> yVals = new ArrayList<Entry>();
//        for(int jk=0;jk<2;jk++){

        if (strdata == null) return false;
        dataString.append(strdata);
        Log.i("test point", "testpoint" + dataString.toString());

        if (dataString.indexOf(ENTER_STRING, 0) != -1) {

            String stringtmp = splitString(dataString.indexOf(ENTER_STRING, 0) - 1);

            Log.d("SLIT", "SA" + stringtmp);
            nam = (stringtmp).split(",");
            for (int j = 1; j < nam.length - 1; j++) {

                if (isNumber(nam[j])) {
                    yVals.add(new Entry(Integer.parseInt(nam[j]), j));
                } else {
                    yVals.add(new Entry(0, j));
                }
                xVals.add(String.valueOf(j + 1));

            }
            if (Integer.parseInt(nam[nam.length - 1]) > 50)
                freq_list.add(Integer.parseInt(nam[nam.length - 1]));
            alertLie();
            freqe.setText(getActivity().getResources().getString(R.string.sinpaku) + nam[nam.length - 1]);

            LineDataSet set1 = new LineDataSet(yVals, getActivity().getResources().getString(R.string.yvals));
            SharedPreferences st = PreferenceManager.getDefaultSharedPreferences(getActivity());


            //波形の色　Color.rgb
            set1.setColor(Color.parseColor("#" + (String.format("%08x", st.getInt("graph_line_color", 0x33b5e5)))));
            set1.setCircleColor(Color.parseColor("#" + (String.format("%08x", st.getInt("graph_line_color", 0x33b5e5)))));
            set1.setLineWidth(2f);
            set1.setCircleSize(4f);
            set1.setFillAlpha(65);
            set1.setFillColor(Color.parseColor("#" + (String.format("%08x", st.getInt("graph_line_color", 0x33b5e5)))));
            set1.setHighLightColor(Color.parseColor("#" + (String.format("%08x", st.getInt("graph_line_color", 0x33b5e5)))));
            mChart.setBackgroundColor(Color.parseColor("#" + (String.format("%08x", st.getInt("graph_gb_color", 0x222222)))));

            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(xVals, dataSets);

            // set data
            mChart.setData(data);
            return true;
        }
        return false;

    }

    /**
     * 送られてきたデータの分割
     *
     * @param index 　切り取る範囲
     * @return　切り取られた文字列たち。
     */
    public String splitString(int index) {
        String[] tmp;
        StringBuilder strb;
        tmp = dataString.toString().split(ENTER_STRING);
        try {
            dataString.delete(0, index + 5);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tmp[0];
    }


    /**
     * チャートの値のｾｯﾃｲ
     */
    public void setChartInfo(String chartData) {
//        Typeface tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");
        // add data
        if (setChartData(chartData) != true) return;

        //レジェンドの設定
        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();

        // modify the legend ...
        // l.setPosition(LegendPosition.LEFT_OF_CHART);
        l.setForm(Legend.LegendForm.LINE);
//        l.setTypeface(tf);
        l.setTextColor(Color.WHITE);

        //X軸の設定
        XLabels xl = mChart.getXLabels();
//        xl.setTypeface(tf);
        xl.setTextColor(Color.WHITE);

        //Yラベルの設定
        YLabels yl = mChart.getYLabels();
        //yl.setTypeface(tf);
        yl.setTextColor(Color.WHITE);
        //X軸のアニメーション時間
        mChart.animateX(100);

        //値のテキストカラー
        mChart.setValueTextColor(Color.WHITE);

        //X軸の値に付くもの(単位)
        mChart.setUnit(" ");
        mChart.setDrawUnitsInChart(true);

        // 有効にすると、グラフの値が0から始まります♪
        mChart.setStartAtZero(true);

        //Y軸の値を無効にします
        mChart.setDrawYValues(false);

        SharedPreferences st = PreferenceManager.getDefaultSharedPreferences(getActivity());
        //Yの値を出力します。
        mChart.setDrawYValues(st.getBoolean("yvalue", false));
        //ハイライトを有効にします。
        mChart.setHighlightEnabled(st.getBoolean("highlight", true));
        //グラフの下に色を付けます
        if (st.getBoolean("fillcolor", false) == true) {
            ArrayList<LineDataSet> sets = (ArrayList<LineDataSet>) mChart.getData()
                    .getDataSets();
            for (LineDataSet set : sets) {
                set.setDrawFilled(true);
            }
        }
        //グラフの点に丸を書きます
        if (st.getBoolean("drawcircle", true) == true) {
            ArrayList<LineDataSet> sets = (ArrayList<LineDataSet>) mChart.getData()
                    .getDataSets();

            for (LineDataSet set : sets) {
                set.setDrawCircles(true);
            }
        } else {
            ArrayList<LineDataSet> sets = (ArrayList<LineDataSet>) mChart.getData()
                    .getDataSets();

            for (LineDataSet set : sets) {
                set.setDrawCircles(false);
            }
        }

        mChart.setDrawBorder(true);
        mChart.setBorderPositions(new BarLineChartBase.BorderPosition[]{
                BarLineChartBase.BorderPosition.BOTTOM
        });
        //グラフの点に四角を書きます
        if (st.getBoolean("drawsquare", false) == true) {
            ArrayList<LineDataSet> sets = (ArrayList<LineDataSet>) mChart.getData()
                    .getDataSets();

            for (LineDataSet set : sets) {
                set.setDrawCubic(true);
            }

        } else {
            ArrayList<LineDataSet> sets = (ArrayList<LineDataSet>) mChart.getData()
                    .getDataSets();

            for (LineDataSet set : sets) {
                set.setDrawCubic(false);
            }
        }
        //グラフのY軸を0から始めます
        mChart.setStartAtZero(st.getBoolean("startatzero", true));
        // 無効にすると、xとyのzoomが分かれます　if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(st.getBoolean("pinchzoom", true));

        // 詳細と、グラフがなかった時の詳細
        mChart.setDescription(getActivity().getResources().getString(R.string.graph_description));
        mChart.setNoDataTextDescription(getActivity().getResources().getString(R.string.graph_nodatadescription));
        mChart.setNoDataText(getActivity().getResources().getString(R.string.graph_nodatatext));

        // 値のハイライトを有効にします
//        mChart.setHighlightEnabled(true);

        // タッチジェスチャーを有効にします
        mChart.setTouchEnabled(true);

        // スケーリングと移動を有効にします
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);
        mChart.setDrawVerticalGrid(false);
        mChart.setDrawHorizontalGrid(false);


    }


    /**
     * 数字チェック
     *
     * @param num 　比較する文字列
     * @return　数字かそうでないか
     */
    public boolean isNumber(String num) {
        try {
            Integer.parseInt(num);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        ((MainActivity) activity).onSectionAttached(getArguments().getInt(
                ARG_SECTION_NUMBER));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, android.view.MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.line, menu);
        return;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (usb != null) usb.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //オプション設定
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.onlyGraph:
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.container,
                                NewChartFragment.newInstance(0), "NewChartFragment")
                        .commit();
                break;
            case R.id.twitterrenkei:
                authClient = new TwitterAuthClient();
                if (getActivity().getSharedPreferences("com.twitter.sdk.android:twitter-core:com.twitter.sdk.android.core.TwitterCore", getActivity().MODE_PRIVATE).getString("active_twittersession", null) == null) {
                    authClient.authorize(getActivity(), new Callback<TwitterSession>() {
                        @Override
                        public void success(Result<TwitterSession> twitterSessionResult) {
                            Log.d("sakusesu", "作成");
//                            mTwitterSession = twitterSessionResult.data;
                            //Activity resultじゃないと受け取れないｗｗ
//                            tweetAdapter();
                            Toast.makeText(getActivity(), "認証しました。", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void failure(TwitterException e) {
                            Log.d("sakusesu", "sippai");
                        }
                    });
                } else {
                    tweetAdapter();
                }
                break;
            case R.id.logout:
                Twitter.getSessionManager().clearActiveSession();
                Toast.makeText(getActivity(), "ログアウトしました。", Toast.LENGTH_SHORT).show();
                break;

        }
        return true;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        authClient.onActivityResult(requestCode, resultCode, intent);

    }

    /**
     * ツイートする。
     *
     */
    private void tweetAdapter() {
        LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View layout = inflater.inflate(R.layout.tweet_disp, (ViewGroup) getActivity().findViewById(R.id.rootviiew));
        AlertDialog.Builder ard = new AlertDialog.Builder(getActivity());
        ((EditText) layout.findViewById(R.id.tweettext)).setText("計測なう #組み込みマイスター");
        ard.setTitle(getActivity().getResources().getString(R.string.nanitui));
        ard.setView(layout);
        ard.setPositiveButton(getActivity().getResources().getString(R.string.tweet), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d("ツイートするお", "suruo");

                tweet(((EditText) layout.findViewById(R.id.tweettext)).getText().toString());


            }
        });
        ard.setNegativeButton(getActivity().getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Log.d("ツイートしないお", "sinaio");

            }
        });
        ard.create().show();

    }

    //ログアウト     Twitter.getSessionManager().clearActiveSession();
    public void tweet(String text) {
        TwitterApiClient twitterApiClient = TwitterCore.getInstance().getApiClient();
        StatusesService statusesService = twitterApiClient.getStatusesService();
        AssetManager asma = getActivity().getResources().getAssets();
        Fabric.with(getActivity(), new TweetComposer());
        File imagefile = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/DCIM/Camera/PANO_20150311_184954.jpg");
        Uri myImageURI = Uri.fromFile(imagefile);
        statusesService.update(text, null, false, null, null, null, false, null, new Callback() {
            @Override
            public void success(Result result) {
                mHandler.post(new Runnable() {
                    public void run() {

                        Toast.makeText(getActivity(), "ツイートしました。", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            public void failure(TwitterException exception) {
                mHandler.post(new Runnable() {
                    public void run() {
                        Toast.makeText(getActivity(), "ツイートに失敗しました。", Toast.LENGTH_SHORT).show();
                    }
                });

            }
        });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save_image:
                ((FloatingActionsMenu) rootview.findViewById(R.id.multiple_actions_up)).collapse();
//                File file=new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/redgrape/pic");
//                if(file.exists()==false) file.mkdir();
                if (mChart.saveToGallery("title" + System.currentTimeMillis(), 100)) {
                    Toast.makeText(getActivity().getApplicationContext(), getActivity().getResources().getString(R.string.savetogalary),
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), getActivity().getResources().getString(R.string.savefailure), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.startstop:
                ((FloatingActionsMenu) rootview.findViewById(R.id.multiple_actions_up)).collapse();
                if (runb == true) {
                    ((FloatingActionButton) rootview.findViewById(R.id.startstop)).setIcon(R.drawable.ic_play_arrow_grey600_36dp);
                    ((FloatingActionButton) rootview.findViewById(R.id.startstop)).setTitle(getActivity().getResources().getString(R.string.start));
                    runb = false;
                } else {
                    ((FloatingActionButton) rootview.findViewById(R.id.startstop)).setIcon(R.drawable.ic_pause_grey600_36dp);
                    ((FloatingActionButton) rootview.findViewById(R.id.startstop)).setTitle(getActivity().getResources().getString(R.string.stop));
                    runb = true;
                }
                break;
            case R.id.reconnect:
                ((FloatingActionsMenu) rootview.findViewById(R.id.multiple_actions_up)).collapse();
                //USBを繋ぐ
                UsbManager manager = (UsbManager) getActivity().getSystemService(Context.USB_SERVICE);
                usb = UsbSerialProber.acquire(manager);
                if (usb != null) {
                    try {
                        Log.i("USB", "IS NOT NULL");
                        serialIoManager = new SerialInputOutputManager(usb, mListener);
                        executor.submit(serialIoManager);
                        usb.open();
                        usb.setBaudRate(9600);
                        Toast.makeText(getActivity().getApplicationContext(), getActivity().getResources().getString(R.string.connected),
                                Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Toast.makeText(getActivity().getApplicationContext(), getActivity().getResources().getString(R.string.unconnect),
                                Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), getActivity().getResources().getString(R.string.error),
                            Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
