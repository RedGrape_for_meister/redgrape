package redgrape.kumikomi;

import android.app.ActionBar;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;

public class MainActivity extends Activity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks {


	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;
//    private Toolbar mToolBar;
	/**
	 * Used to store the last screen title. For use in
	 *
	 */
	private CharSequence mTitle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main);

        getActionBar().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.actionbar_red)));

//昔の実装
		mNavigationDrawerFragment = (NavigationDrawerFragment) getFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));


		mTitle = getTitle();
		// Set up the drawer.

	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// update the main content by replacing fragments
		if (position == 1) {
            Intent intent = new Intent();
            intent.setClass(getApplicationContext(), SettingsActivity.class);
            startActivity(intent);
//        }else if(position == 1) {
//            FragmentManager fragmentManager = getFragmentManager();
//            fragmentManager
//                    .beginTransaction()
//                    .replace(R.id.container,
//                            NewChartFragment.newInstance(position + 1))
//                    .commit();

        } else if(position == 0){
            FragmentManager fragmentManager = getFragmentManager();
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.container,
                            NewDetectorFragment.newInstance(position + 1),"NewDetectorFragment")
                    .commit();
		} else {
			FragmentManager fragmentManager = getFragmentManager();
			fragmentManager
					.beginTransaction()
					.replace(R.id.container,
							PlaceholderFragment.newInstance(position + 1))
					.commit();
		}
	}

	public void onSectionAttached(int number) {
		switch (number) {
		case 1:
			mTitle = getString(R.string.title_section1);
			break;
//		case 2:
//			mTitle = getString(R.string.title_section2);
//			break;
		case 2:
			mTitle = getString(R.string.title_section3);
			break;
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass the activity result to the fragment, which will
        // then pass the result to the login button.
        Fragment fragment = getFragmentManager()
                .findFragmentByTag("NewDetectorFragment");
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
        Fragment fragment2 = getFragmentManager()
                .findFragmentByTag("NewChartFragment");
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

}
