package redgrape.kumikomi;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.github.mikephil.charting.charts.BarLineChartBase;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.Legend;
import com.github.mikephil.charting.utils.XLabels;
import com.github.mikephil.charting.utils.YLabels;
import com.hoho.android.usbserial.driver.UsbSerialDriver;
import com.hoho.android.usbserial.driver.UsbSerialProber;
import com.hoho.android.usbserial.util.SerialInputOutputManager;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import io.fabric.sdk.android.Fabric;

/**
 * A placeholder fragment containing a simple view.
 */
public class NewChartFragment extends Fragment implements View.OnClickListener {
    /**
     * The fragment argument representing the section number for this fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";
    private static View rootview;
    private Thread mThread;
    private static boolean runb = true;
    public static String ENTER_STRING = ",end";


    /**
     *
     */
    //通信関連
    private SerialInputOutputManager serialIoManager;
    private final ExecutorService executor = Executors.newSingleThreadExecutor();
    /**
     * データのbuffer
     */
    public static StringBuilder dataString = new StringBuilder();
    public static boolean flagsdata = false;
    public static StringBuilder dataStringTMP = new StringBuilder();

    private int SLEEP_TIME = 1000;//threadのスリープする時間。アニメーション時間より長めに
    /**
     * USB通信
     */
    private UsbSerialDriver usb;

    /**
     * チャート
     */
    private LineChart mChart;
    private Handler mHandler = new Handler();

    TextView si;

    /**
     * Returns a new instance of this fragment for the given section number.
     */
    public static NewChartFragment newInstance(int sectionNumber) {
        NewChartFragment fragment = new NewChartFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    /**
     * シリアル入出力 Listener
     */
    private final SerialInputOutputManager.Listener mListener =
            new SerialInputOutputManager.Listener() {
                @Override
                public void onRunError(Exception e) {
                    try {
                        usb.close();
                    } catch (IOException e1) {
                    }
                    serialIoManager.stop();
                }

                public void onNewData(final byte[] data) {
                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            while (runb == false) ;
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    // add data
                                    try {
                                        setChartInfo(new String(data, "US-ASCII"));
                                    } catch (UnsupportedEncodingException e) {
                                        e.printStackTrace();
                                    }
                                    mChart.animateX(100);

                                }
                            });
                        }

                    });
                    t.run();
                }
            };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        // TODO 自動生成されたメソッド・スタブ
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        //USBを繋ぐ
        UsbManager manager = (UsbManager) getActivity().getSystemService(Context.USB_SERVICE);
        usb = UsbSerialProber.acquire(manager);
        if (usb != null) {
            try {
                //listener　設定用
//                Log.i("USB","IS NOT NULL");
                serialIoManager = new SerialInputOutputManager(usb, mListener);
                executor.submit(serialIoManager);
                usb.open();
                usb.setBaudRate(9600);
//                runThread(); // シリアル通信を読むスレッドを起動
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        rootview = inflater.inflate(R.layout.activity_linechart, container, false);
        ((FloatingActionButton) rootview.findViewById(R.id.save_image)).setOnClickListener(this);
        ((FloatingActionButton) rootview.findViewById(R.id.startstop)).setOnClickListener(this);
        ((FloatingActionButton) rootview.findViewById(R.id.reconnect)).setOnClickListener(this);
        mChart = (LineChart) rootview.findViewById(R.id.chart1);
        mChart.setNoDataTextDescription(getActivity().getResources().getString(R.string.graph_nodatadescription));
        mChart.setNoDataText(getActivity().getResources().getString(R.string.graph_nodatatext));
        si = (TextView) rootview.findViewById(R.id.sinpaku_value);

        setChartInfo("0,0\n");
        return rootview;
    }


    /**
     * チャートデータをセットします
     */
    private boolean setChartData(String strdata) {

        String[] nam;
        ArrayList<String> xVals = new ArrayList<String>();
        ArrayList<Entry> yVals = new ArrayList<Entry>();


        if (strdata == null) return false;
        dataString.append(strdata);
        Log.i("test point", "testpoint" + dataString.toString());

        if (dataString.indexOf(ENTER_STRING, 0) != -1) {

            String stringtmp = splitString(dataString.indexOf(ENTER_STRING, 0) - 1);

            Log.d("SLIT", "SA" + stringtmp);
            nam = (stringtmp).split(",");
            for (int j = 1; j < nam.length - 1; j++) {

                if (isNumber(nam[j])) {
                    yVals.add(new Entry(Integer.parseInt(nam[j]), j));
                } else {
                    yVals.add(new Entry(0, j));
                }
                xVals.add(String.valueOf(j + 1));

            }
            si.setText(getActivity().getResources().getString(R.string.sinpaku) + nam[nam.length - 1]);

            LineDataSet set1 = new LineDataSet(yVals, getActivity().getResources().getString(R.string.yvals));
            SharedPreferences st = PreferenceManager.getDefaultSharedPreferences(getActivity());


            //波形の色　Color.rgb
            set1.setColor(Color.parseColor("#" + (String.format("%08x", st.getInt("graph_line_color", 0x33b5e5)))));
            set1.setCircleColor(Color.parseColor("#" + (String.format("%08x", st.getInt("graph_line_color", 0x33b5e5)))));
            set1.setLineWidth(2f);
            set1.setCircleSize(4f);
            set1.setFillAlpha(65);
            set1.setFillColor(Color.parseColor("#" + (String.format("%08x", st.getInt("graph_line_color", 0x33b5e5)))));
            set1.setHighLightColor(Color.parseColor("#" + (String.format("%08x", st.getInt("graph_line_color", 0x33b5e5)))));
            mChart.setBackgroundColor(Color.parseColor("#" + (String.format("%08x", st.getInt("graph_gb_color", 0x33b5e5)))));

            ArrayList<LineDataSet> dataSets = new ArrayList<LineDataSet>();
            dataSets.add(set1); // add the datasets

            // create a data object with the datasets
            LineData data = new LineData(xVals, dataSets);

            // set data
            mChart.setData(data);
            return true;
        }
        return false;

    }

    /**
     * 送られてきたデータの分割
     *
     * @param index 　切り取る範囲
     * @return　切り取られた文字列たち。
     */
    public String splitString(int index) {
        String[] tmp;
        tmp = dataString.toString().split(ENTER_STRING);
        try {
            dataString.delete(0, index + 5);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tmp[0];
    }


    /**
     * チャートの値のｾｯﾃｲ
     */
    public void setChartInfo(String chartData) {
//        Typeface tf = Typeface.createFromAsset(getAssets(), "OpenSans-Regular.ttf");
        // add data
        if (setChartData(chartData) != true) return;

        //レジェンドの設定
        // get the legend (only possible after setting data)
        Legend l = mChart.getLegend();

        // modify the legend ...
        // l.setPosition(LegendPosition.LEFT_OF_CHART);
        l.setForm(Legend.LegendForm.LINE);
//        l.setTypeface(tf);
        l.setTextColor(Color.WHITE);

        //X軸の設定
        XLabels xl = mChart.getXLabels();
//        xl.setTypeface(tf);
        xl.setTextColor(Color.WHITE);

        //Yラベルの設定
        YLabels yl = mChart.getYLabels();
        //yl.setTypeface(tf);
        yl.setTextColor(Color.WHITE);
        //X軸のアニメーション時間
        mChart.animateX(100);

        //値のテキストカラー
        mChart.setValueTextColor(Color.WHITE);

        //X軸の値に付くもの(単位)
        mChart.setUnit(" ");
        mChart.setDrawUnitsInChart(true);

        // 有効にすると、グラフの値が0から始まります♪
        mChart.setStartAtZero(true);

        //Y軸の値を無効にします
        mChart.setDrawYValues(false);

        SharedPreferences st = PreferenceManager.getDefaultSharedPreferences(getActivity());
        //Yの値を出力します。
        mChart.setDrawYValues(st.getBoolean("yvalue", false));
        //ハイライトを有効にします。
        mChart.setHighlightEnabled(st.getBoolean("highlight", true));
        //グラフの下に色を付けます
        if (st.getBoolean("fillcolor", false) == true) {
            ArrayList<LineDataSet> sets = (ArrayList<LineDataSet>) mChart.getData()
                    .getDataSets();
            for (LineDataSet set : sets) {
                set.setDrawFilled(true);
            }
        }
        //グラフの点に丸を書きます
        if (st.getBoolean("drawcircle", true) == true) {
            ArrayList<LineDataSet> sets = (ArrayList<LineDataSet>) mChart.getData()
                    .getDataSets();

            for (LineDataSet set : sets) {
                set.setDrawCircles(true);
            }
        }

        mChart.setDrawBorder(true);
        mChart.setBorderPositions(new BarLineChartBase.BorderPosition[]{
                BarLineChartBase.BorderPosition.BOTTOM
        });
        //グラフの点に四角を書きます
        if (st.getBoolean("drawsquare", false) == true) {
            ArrayList<LineDataSet> sets = (ArrayList<LineDataSet>) mChart.getData()
                    .getDataSets();

            for (LineDataSet set : sets) {
                set.setDrawCubic(true);
            }

        }
        //グラフのY軸を0から始めます
        mChart.setStartAtZero(st.getBoolean("startatzero", true));
        // 無効にすると、xとyのzoomが分かれます　if disabled, scaling can be done on x- and y-axis separately
        mChart.setPinchZoom(st.getBoolean("pinchzoom", true));

        // 詳細と、グラフがなかった時の詳細
        mChart.setDescription(getActivity().getResources().getString(R.string.graph_description));
        mChart.setNoDataTextDescription(getActivity().getResources().getString(R.string.graph_nodatadescription));
        mChart.setNoDataText(getActivity().getResources().getString(R.string.graph_nodatatext));

        // 値のハイライトを有効にします
//        mChart.setHighlightEnabled(true);

        // タッチジェスチャーを有効にします
        mChart.setTouchEnabled(true);

        // スケーリングと移動を有効にします
        mChart.setDragEnabled(true);
        mChart.setScaleEnabled(true);
        mChart.setDrawGridBackground(false);
        mChart.setDrawVerticalGrid(false);
        mChart.setDrawHorizontalGrid(false);


    }


    /**
     * 数字チェック
     *
     * @param num 　比較する文字列
     * @return　数字かそうでないか
     */
    public boolean isNumber(String num) {
        try {
            Integer.parseInt(num);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        ((MainActivity) activity).onSectionAttached(getArguments().getInt(
                ARG_SECTION_NUMBER));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, android.view.MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.backdetector, menu);
        return;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        try {
            if (usb != null) usb.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    //オプション設定
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.backDetector:
                FragmentManager fragmentManager = getFragmentManager();
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.container,
                                NewDetectorFragment.newInstance(0),"NewDetectorFragment")
                        .commit();
                break;
//            case R.id.twitterrenkei:
//                TwitterAuthConfig authConfig = new TwitterAuthConfig(NewDetectorFragment.TWITTER_KEY, NewDetectorFragment.TWITTER_SECRET);
//                Fabric.with(getActivity(), new Twitter(authConfig));
//                TwitterAuthClient authClient = new TwitterAuthClient();
//                authClient.authorize(getActivity(), new Callback<TwitterSession>() {
//                    @Override
//                    public void success(Result<TwitterSession> twitterSessionResult) {
//                        Log.d("sakusesu","作成");
//                        //Activity resultじゃないと受け取れないｗｗ
//                    }
//
//                    @Override
//                    public void failure(TwitterException e) {
//                        Log.d("sakusesu","sippai");
//                    }
//                });
//                break;

        }
        return true;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.save_image:
                ((FloatingActionsMenu) rootview.findViewById(R.id.multiple_actions_up)).collapse();
//                File file=new File(Environment.getExternalStorageDirectory().getAbsolutePath()+"/redgrape/pic");
//                if(file.exists()==false) file.mkdir();
                if (mChart.saveToGallery("title" + System.currentTimeMillis(), 100)) {
                    Toast.makeText(getActivity().getApplicationContext(), getActivity().getResources().getString(R.string.savetogalary),
                            Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), getActivity().getResources().getString(R.string.savefailure), Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.startstop:
                ((FloatingActionsMenu) rootview.findViewById(R.id.multiple_actions_up)).collapse();
                if (runb == true) {
                    ((FloatingActionButton) rootview.findViewById(R.id.startstop)).setIcon(R.drawable.ic_play_arrow_grey600_36dp);
                    ((FloatingActionButton) rootview.findViewById(R.id.startstop)).setTitle(getActivity().getResources().getString(R.string.start));
                    runb = false;
                } else {
                    ((FloatingActionButton) rootview.findViewById(R.id.startstop)).setIcon(R.drawable.ic_pause_grey600_36dp);
                    ((FloatingActionButton) rootview.findViewById(R.id.startstop)).setTitle(getActivity().getResources().getString(R.string.stop));
                    runb = true;
                }
                break;
            case R.id.reconnect:
                ((FloatingActionsMenu) rootview.findViewById(R.id.multiple_actions_up)).collapse();
                //USBを繋ぐ
                UsbManager manager = (UsbManager) getActivity().getSystemService(Context.USB_SERVICE);
                usb = UsbSerialProber.acquire(manager);
                if (usb != null) {
                    try {
                        Log.i("USB", "IS NOT NULL");
                        serialIoManager = new SerialInputOutputManager(usb, mListener);
                        executor.submit(serialIoManager);
                        usb.open();
                        usb.setBaudRate(9600);
                        Toast.makeText(getActivity().getApplicationContext(), getActivity().getResources().getString(R.string.connected),
                                Toast.LENGTH_SHORT).show();
                    } catch (Exception e) {
                        Toast.makeText(getActivity().getApplicationContext(), getActivity().getResources().getString(R.string.unconnect),
                                Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(getActivity().getApplicationContext(), getActivity().getResources().getString(R.string.error),
                            Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
