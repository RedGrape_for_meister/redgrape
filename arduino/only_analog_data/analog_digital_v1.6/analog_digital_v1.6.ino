#define RS 50  // Read Speed [ms]:この時間毎にデジタルおよびアナログピンのデータを読む
#define RT 20  // まとめて送るデータの数:RS×RTがデータを送る間隔
#define FC 2   // ノイズ対策[回]:周波数計測の時，digitalReadの値がこの回数連続した場合，変更したとする
#define FN 10   // 周波数はこの回数だけ平均を取って送る

int freq[FN] = {};   // 直近の周波数を格納
int freqave;         // 直近の平均周波数を格納．小数点型を使うと多少計算に時間がかかるので整数型を使用

void setup(){
  pinMode(5,INPUT);
  pinMode(14,INPUT);
  Serial.begin(9600);
}

// 最初だけ，起動してから初めて1がFCだけ連続するまでの時間をfreq[0]に格納
// その後はfreq[0]～freq[FN-1]に，順に周波数を格納していく
// 最初に正しい周波数が出るのはFN周期目
void freqchek(){  
  static int fflag1 = 0,fflag0 = 0;  // 1,0が連続した回数を格納
  static int fl = 0;  /* チェックの状態:1が初めてFCだけ連続したら1，
                         その後初めて0がFCだけ連続したら0に戻る．*/
  static int cdd;  // Current digital data:現在のデジタルデータ[ 0 or 1 ]
  static int obt = 0;  // One before time[ms]:前回1がFC連続した時の時刻
  static int cnt = 0;
  cdd = digitalRead(5);
  if (cdd) {
    fflag1++;
    fflag0 = 0;
    if (fflag1>=FC && !fl) {
      freq[cnt] = millis() - obt;  // 測定終了
      obt = millis();  // 測定開始
      fl++;
    }
  } else {
    fflag0++;
    fflag1 = 0;
    if (fflag0>=FC && fl) fl--;
  }
  cnt = (cnt + 1) % FN;  // freqの要素を循環的に使う．cntを0～FN-1で循環させる
  for (int j=0;j<FN;j++) freqave += freq[j];
  freqave /= FN;
}

void loop(){
  static int pre = RT - 1;
  static int i = 0;
  static String strAnadata;
  String tmp = "",pulse = "";
  int ftmp;
  freqchek();  /* 周波数算出プログラム */
  tmp=String(analogRead(0),DEC);
  if (i==0) {
    strAnadata = tmp;
    strAnadata += ",";
    delay(10);
    do {
      delayMicroseconds(10);
    } while (millis() % RS);
  } else if (i==RT) {
    strAnadata += tmp;
    strAnadata += ",";
    ftmp = 60000 / freqave;  // 周波数を脈拍数[回/min]にする
    ftmp = constrain(ftmp, 40, 250);  // 脈拍の値を40～250の範囲に収める．
    pulse = String(ftmp,DEC);
    pulse += ",end";
    delay(10);
    do {
      delayMicroseconds(10);
    } while (millis() % RS);
    /* プログラムを一時停止する．起動してからRS[ms]ごとに抜けだして一周動作する．
       millis()はプログラムの実行を開始した時から現在までの時間をミリ秒単位で返す．約50日でオーバーフロー．
       これで正確にRS＊RT[ms]ごとにデータを送信できるはず．
       代案：do {delayMicroseconds(4);} while ((micros()/8)%(RS*125));  micros()はマイクロ秒単位；
    */
    Serial.println(strAnadata + pulse);
    // [data1,data2,...,dataRT,data_pulse,end]を改行と一緒に送る．String型．
    i = -1;
  } else {
    strAnadata += tmp;
    strAnadata += ",";
    pre = i;
    delay(10);
    do {
      delayMicroseconds(10);
    } while (millis() % RS);
  }
  i++;
  //int i;
  /*
  int ana_n,dig_n;
  ana_n=analogRead(0);
  dig_n=digitalRead(5);
  Serial.print(ana_n);
  Serial.print(",");
  Serial.print(dig_n);
  Serial.print("\n");
  delay(10);
  */
}
